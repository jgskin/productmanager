const API = 'http://localhost:8000/api'

export function fetchProductList() {
  return fetch(`${API}/products`).then(res => res.json())
}

export function fetchProduct(id) {
  return fetch(`${API}/products/${id}`).then(res => res.json())
}

export function updateProduct(product) {
  return fetch(`${API}/products/${product.id}`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(product),
  })
}

export function deleteProduct(id) {
  return fetch(`${API}/products/${id}`, {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
  })
}

export function createProduct(data) {
  return fetch(`${API}/products`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  })
}