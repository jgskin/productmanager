import React, { useEffect, useState } from 'react'
import { fetchProductList } from '../api'
import Product from './Product'

function ProductList(props) {
  const [ products, setProducts ] = useState([])

  useEffect(() => {
    fetchProductList().then(json => setProducts(json))
  }, [])

  return (
    <div className="container">
      <div className="product-list">
        {products.map(product => (
          <Product key={product.id} {...product} />
        ))}
      </div>
    </div>
  )
}

export default ProductList