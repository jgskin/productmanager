import React from 'react'
import ProductForm, { DEFAULT_STATE } from './ProductForm'
import { createProduct } from '../api'

class ProductEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      product: { ...DEFAULT_STATE }
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleDataChange = this.handleDataChange.bind(this)
  }

  handleSubmit() {
    createProduct(this.state.product)
      .then(res => {
        this.props.onUserMessage('Produto Criado!')
        this.props.history.push('/')
      })
  }

  handleDataChange(data) {
    this.setState({ product: { ...this.state.product, ...data } })
  }

  render() {
    return (
      <ProductForm 
        data={this.state.product}
        onSubmit={this.handleSubmit}
        onDataChange={this.handleDataChange}>
        <div>
          <button className="bt bt-blue" type="submit">Salvar</button>
        </div>
      </ProductForm>
    )
  }
}

export default ProductEdit;