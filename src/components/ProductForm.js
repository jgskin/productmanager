import React from 'react'
import { formatCurrency } from '../helper'

const DEFAULT_STATE = {
  name: '',
  description: '',
  price: 0,
}

function ProductForm(props)  {
  const { children, data, onSubmit, onDataChange } = props
  const { name, description, price } = data

  const handleInputChange = ({ target }) => {
    let { name, value } = target

    if (name === 'price') {
      value = parseInt(value.replace(/\D/gi, ''))
    }
    
    onDataChange({ [name]: value });
  }

  const handleSubmit = event => {
    event.preventDefault()

    onSubmit()
  }

  return (
    <div id="product-form">
      <form className="form-container" onSubmit={handleSubmit}>
        
        <label>
          Nome
          <input 
              type="text"
              name="name"
              value={name}
              onChange={handleInputChange} />
        </label>

        <label>
          Preço
          <input 
            type="text"
            name="price"
            value={formatCurrency(price)}
            onChange={handleInputChange} />
        </label>

        <label>
          Descrição
          <textarea 
            type="text"
            name="description"
            value={description}
            onChange={handleInputChange} />
        </label>

        {children}
        
      </form>
    </div>
  )
}

export { DEFAULT_STATE }
export default ProductForm