import React from 'react'
import { Link } from 'react-router-dom'

function Navbar(props) {
  return (
    <header>
      <div className="container">
        <Link to="/">Listar Produtos</Link>
        <Link to="/criar">Criar Produto</Link>
      </div>
    </header>
  )
}

export default Navbar