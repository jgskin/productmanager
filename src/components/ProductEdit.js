import React from 'react'
import ProductForm, { DEFAULT_STATE } from './ProductForm'
import { deleteProduct, fetchProduct, updateProduct } from '../api'

class ProductEdit extends React.Component {
  constructor(props) {
    super(props)

    this.productID = this.props.match.params.id

    this.state = {
      product: { ...DEFAULT_STATE }
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleDataChange = this.handleDataChange.bind(this)
  }

  componentWillUnmount() {
    this.setState({ product: { ...DEFAULT_STATE } })
  }

  componentDidMount() {
    fetchProduct(this.productID)
      .then(product => this.setState({ product }))
  }

  handleSubmit() {
    updateProduct(this.state.product)
      .then(res => {
        this.props.onUserMessage('Produto atualizado!')
        this.props.history.push('/')
      })
  }

  handleDelete() {
    deleteProduct(this.productID).then(() => {
      this.props.onUserMessage('Produto deletado!')
      this.props.history.push('/')
    });
  }

  handleDataChange(data) {
    this.setState({ product: { ...this.state.product, ...data } })
  }

  render() {
    return (
      <ProductForm 
        data={this.state.product}
        onSubmit={this.handleSubmit}
        onDataChange={this.handleDataChange}>
        <div>
          <button className="bt bt-blue" type="submit">Salvar</button>
          <button 
            className="bt bt-red"
            type="button"
            onClick={this.handleDelete}>Deletar</button>
        </div>
      </ProductForm>
    )
  }
}

export default ProductEdit;