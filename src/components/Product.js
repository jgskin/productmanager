import React from 'react'
import { Link } from 'react-router-dom'
import { formatCurrency } from '../helper'

const IMGURL = 'https://placekitten.com/300/300?image='

function Product({ id, name, description, price, imgPath }) {
  return (
    <Link to={`/${id}`} className="product">
      <img src={IMGURL + Math.floor(Math.random() * 17)} alt={name} />
      
      <div className="product-details">
        <h4>{name}</h4>
        <span>{formatCurrency(price)}</span>
      </div>
      
      <div className="product-description">
        <div className="product-details">
          <h3>{name}</h3>
          <span>{formatCurrency(price)}</span>
        </div>
        <p>
          {description}
        </p>
      </div>
    </Link>
  )
}

export default Product