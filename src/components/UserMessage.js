import React from 'react';

function UserMessage({ message, onShowTimeout }) {

  setTimeout(onShowTimeout, 5000)

  return (
    <div className={`user-message ${message ? 'active' : ''}`}>
      <p>{message}</p>
    </div>
  )

}

export default UserMessage