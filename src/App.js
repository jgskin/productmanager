import React, { useState } from 'react'
import { Route, Switch } from 'react-router-dom'
import Navbar from './components/Navbar'
import ProductList from './components/ProductList'
import ProductEdit from './components/ProductEdit'
import ProductCreate from './components/ProductCreate'
import UserMessage from './components/UserMessage'
import './App.css'

function App() {
  const [ userMessage, setUserMessage ] = useState(null)

  return (
    <div id="product-manager">
      <Navbar />
      <Switch>
        <Route path="/" component={ProductList} exact />
        <Route
          exact 
          path="/criar" 
          render={props => <ProductCreate onUserMessage={setUserMessage} { ...props } />} />
        <Route 
          path="/:id" 
          render={props => <ProductEdit onUserMessage={setUserMessage} { ...props } />} />
      </Switch>
      <UserMessage message={userMessage} onShowTimeout={() => setUserMessage(null)} />
    </div>
  )
}

export default App
