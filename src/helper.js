const formatter = new Intl.NumberFormat('pt-BR', {
  style: 'currency',
  currency: 'BRL',
  minimumFractionDigits: 2
})

export function formatCurrency(currency) {
  return formatter.format(currency / 100)
}